class Fiende:
    def __init__(self, namn, liv, skada):
        self.namn = namn
        self.liv = liv
        self.skada = skada

    def lever(self):
        return self.liv > 0

class Jättespindel(Fiende):
    def __init__(self):
        super().__init__(namn="jättespindel", liv=10, skada=2)

class Troll(Fiende):
    def __init__(self):
        super().__init__(namn="troll", liv=30, skada=15)
