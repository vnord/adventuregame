#import spelare
#import Spelare
from spelare import Spelare

class Handling():
    def __init__(self, method, namn, kortkommando, **kwargs):
        self.method = method
        self.kortkommando = kortkommando
        self.namn = namn
        self.kwargs = kwargs

    def __str__(self):
        return "{}: {}".format(self.kortkommando, self.namn)

class FlyttaNorr(Handling):
    def __init__(self):
        super().__init__(method=Spelare.flytta_norr, namn='Gå norrut', kortkommando='n')


class FlyttaSyd(Handling):
    def __init__(self):
        super().__init__(method=Spelare.flytta_syd, namn='Gå söderut', kortkommando='s')


class FlyttaVäst(Handling):
    def __init__(self):
        super().__init__(method=Spelare.flytta_väst, namn='Gå västerut', kortkommando='v')


class FlyttaÖst(Handling):
    def __init__(self):
        super().__init__(method=Spelare.flytta_öst, namn='Gå österut', kortkommando='ö')

class KollaGrejer(Handling):
    """Visar spelarens grejer"""
    def __init__(self):
        super().__init__(method=Spelare.lista_grejer, namn='Kolla igenom ryggsäcken', kortkommando='g')

class Attackera(Handling):
    def __init__(self, fiende):
        super().__init__(method=Spelare.attackera, namn="Attackera", kortkommando='a', fiende=fiende)

class Fly(Handling):
    def __init__(self, ruta):
        super().__init__(method=Spelare.fly, namn="Fly", kortkommando='f', ruta=ruta)
