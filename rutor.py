import saker, fiender, handlingar, värld

class KartRuta:
    def __init__(self, x, y):
        self.x = x
        self.y = y 

    def intro_text(self):
        raise NotImplementedError()

    def modifiera_spelare(self, spelare):
        raise NotImplementedError()

    def närliggande_förflyttningar(self):
        """Returnerar alla förflyttningshandlingar för närliggande rutor."""
        förflyttningar = []
        if värld.ruta_existerar(self.x + 1, self.y):
            förflyttningar.append(handlingar.FlyttaÖst())
        if värld.ruta_existerar(self.x - 1, self.y):
            förflyttningar.append(handlingar.FlyttaVäst())
        if värld.ruta_existerar(self.x, self.y - 1):
            förflyttningar.append(handlingar.FlyttaNorr())
        if värld.ruta_existerar(self.x, self.y + 1):
            förflyttningar.append(handlingar.FlyttaSyd())
        return förflyttningar

    def tillgängliga_handlingar(self):
        """Returnerar alla möjliga handlingar i detta rum."""
        förflyttningar = self.närliggande_förflyttningar()
        förflyttningar.append(handlingar.KollaGrejer())

        return förflyttningar

class StartRum(KartRuta):
    def intro_text(self):
        return """
        Du befinner dig i en mörk grotta med en fackla på ena väggen.
        Du ser fyra utvägar, var och en lika mörk och hotfull.
        """
        
    def modifiera_spelare(self, spelare):
        #Rummet har ingen händelse för spelaren
        pass

class SkattRum(KartRuta):
    def __init__(self, x, y, sak):
        self.sak = sak
        super().__init__(x, y)

    def ta_skatt(self, spelare):
        player.grejer.append(self.sak)

    def modifiera_spelare(self, spelare):
        self.ta_skatt(spelare)

class FiendeRum(KartRuta):
    def __init__(self, x, y, fiende):
        self.fiende = fiende
        super().__init__(x, y)

    def modifiera_spelare(self, spelaren):
        if self.fiende.lever():
            spelaren.liv = spelaren.liv - self.fiende.skada
            print("Fienden gör {} skada. Du har {} liv kvar.".format(self.fiende.skada, spelaren.liv))
            
    def tillgängliga_handlingar(self):
        if self.fiende.lever():
            return [handlingar.Fly(ruta=self), handlingar.Attackera(fiende=self.fiende)]
        else:
            return self.närliggande_förflyttningar()

class GrottTunnel(KartRuta):
    def intro_text(self):
        return """
        En intentsägande del av grottan. Du måste fortsätta vidare.
        """

    def modifiera_spelare(self, spelare):
        #Rummet har ingen händelse för spelaren
        pass

class JättespindelRum(FiendeRum):
    def __init__(self, x, y):
        super().__init__(x, y, fiender.Jättespindel())

    def intro_text(self):
        if self.fiende.lever():
            return """
            En jättespinder hoppar ner från sin väv framför dig!
            """
        else:
            return """
            Det ligger ett spindellik och ruttnar på marken.
            """

class HittaDolkRum(SkattRum):
    def __init__(self, x, y):
        super().__init__(x, y, saker.Dolk())

    def intro_text(self):
        return """
        Du lägger märke till något som glänser i hörnet.
        Det är en dolk! Du plockar upp den.
        """

class UtrymningsRum(KartRuta):
    def intro_text(self):
        return """
        Du ser någonting ljust lite längre fram...
        ... det blir ljusare allt eftersom du närmare dig!
        Det är solljus!

        Du har vunnit!
        """

    def modifiera_spelare(self, spelare):
        spelare.vunnit = True
