class Sak():
    """Grundklass för alla saker"""
    def __init__(self, namn, beskrivning, värde):
        self.namn = namn
        self.beskrivning = beskrivning
        self.värde = värde

    def __str__(self):
        return "{}\n=====\n{}\nVärde: {}\n".format(self.namn, self.beskrivning, self.värde)

class Guldmynt(Sak):
    def __init__(self, mängd):
        self.mängd = mängd
        super().__init__(namn="Guldmynt",
                         beskrivning="Ett runt guldmynt med {} stämplat på ena sidan.".format(str(self.mängd)),
                         värde=self.mängd)

class Vapen(Sak):
    def __init__(self, namn, beskrivning, värde, skada):
        self.skada = skada
        super().__init__(namn, beskrivning, värde)

    def __str__(self):
        return "{}\n=====\n{}\nVärde: {}\nSkada: {}".format(self.namn, self.beskrivning, self.värde, self.skada)

class Sten(Vapen):
    def __init__(self):
        super().__init__(namn="Sten",
                         beskrivning="En knytnävsstor sten som skulle göra ont att få i huvudet.",
                         värde=0,
                         skada=5)

class Dolk(Vapen):
    def __init__(self):
        super().__init__(namn="Dolk",
                         beskrivning="En liten dolk som rostat lite. Något farligare än en sten.",
                         värde=10,
                         skada=10)

