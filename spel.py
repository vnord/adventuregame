import värld
from spelare import Spelare

def spela():
    värld.ladda_rutor()
    spelare = Spelare()
    rum = värld.ruta_existerar(spelare.plats_x, spelare.plats_y)
    print(rum.intro_text())
    while spelare.lever() and not spelare.vunnit:
        rum = värld.ruta_existerar(spelare.plats_x, spelare.plats_y)
        rum.modifiera_spelare(spelare)
        # Kolla igen eftersom rummet kan ha ändrat spelarens tillstånd
        if spelare.lever() and not spelare.vunnit:
            print("Välj en handling:\n")
            tillgängliga_handlingar = rum.tillgängliga_handlingar()
            for handling in tillgängliga_handlingar:
                print(handling)
            handlings_input = input('Handling: ')
            for handling in tillgängliga_handlingar:
                if handlings_input == handling.kortkommando:
                    spelare.utfor_handling(handling, **handling.kwargs)
                    break

if __name__ == "__main__":
    spela()


