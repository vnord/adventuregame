import random
import saker, värld

class Spelare():
    def __init__(self):
        self.grejer = [saker.Guldmynt(15), saker.Sten()]
        self.liv = 100
        self.plats_x, self.plats_y = värld.start_position
        self.vunnit = False

    def lever(self):
        return self.liv > 0

    def lista_grejer(self):
        for sak in self.grejer:
            print(sak, '\n')

    def flytta(self, dx, dy):
        self.plats_x += dx
        self.plats_y += dy
        print(värld.ruta_existerar(self.plats_x, self.plats_y).intro_text())

    def flytta_väst(self):
        self.flytta(dx=-1, dy=0)

    def flytta_norr(self):
        self.flytta(dx=0, dy=-1)

    def flytta_syd(self):
        self.flytta(dx=0, dy=1)

    def flytta_öst(self):
        self.flytta(dx=1, dy=0)


    def attackera(self, fiende):
        basta_vapen = None
        max_skada = 0
        for i in self.grejer:
            if isinstance(i, saker.Vapen):
                if i.skada > max_skada:
                    max_skada = i.skada
                    basta_vapen = i

        print("Du använder {} mot {}!".format(basta_vapen.namn, fiende.namn))
        fiende.liv -= basta_vapen.skada 
        if not fiende.lever():
            print("Du dödade {}!".format(fiende.namn))
        else:
            print("{} har {} liv kvar.".format(fiende.namn, fiende.liv))

    def utfor_handling(self, handling, **kwargs):
        handlingsmetod = getattr(self, handling.method.__name__)
        if handlingsmetod:
            handlingsmetod(**kwargs)

    def fly(self, ruta):
        """Förflyttar spelaren till en slumpmässig närliggande ruta"""
        tillgängliga_förflyttningar = ruta.närliggande_förflyttningar()
        r = random.randint(0, len(tillgängliga_förflyttningar) - 1)
        self.utfor_handling(tillgängliga_förflyttningar[r])
