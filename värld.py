_värld = {}
start_position = (0, 0)
 
def ladda_rutor():
    """Parses a file that describes the world space into the _world object"""
    with open('karta.txt', 'r') as f:
        rows = f.readlines()
    x_max = len(rows[0].split('\t')) # Assumes all rows contain the same number of tabs
    for y in range(len(rows)):
        cols = rows[y].split('\t')
        for x in range(x_max):
            tile_name = cols[x].replace('\n', '') # Windows users may need to replace '\r\n'
            if tile_name == 'StartRum':
                global start_position
                start_position = (x, y)
            _värld[(x, y)] = None if tile_name == '' else getattr(__import__('rutor'), tile_name)(x, y)

def ruta_existerar(x, y):
    return _värld.get((x, y))
